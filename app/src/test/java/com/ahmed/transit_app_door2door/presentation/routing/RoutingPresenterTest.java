package com.ahmed.transit_app_door2door.presentation.routing;

import com.ahmed.transit_app_door2door.data.api.RoutingResponse;
import com.ahmed.transit_app_door2door.rx.RunOn;
import com.ahmed.transit_app_door2door.usecase.routing.RoutingUseCase;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import io.reactivex.Flowable;
import io.reactivex.schedulers.TestScheduler;

import static com.ahmed.transit_app_door2door.utils.Constants.JSON_DATA;
import static com.ahmed.transit_app_door2door.utils.Constants.PLEASE_CHECK_INTERNET;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 * Created by Ahmed Kamal on 19/12/2017.
 */
public class RoutingPresenterTest
{
    private RoutingResponse response = new Gson().fromJson(JSON_DATA , RoutingResponse.class);

    @Mock
    RoutingUseCase routingUseCase;

    @Mock
    RoutingContract.RoutingView view;

    private TestScheduler testScheduler;

    @InjectMocks RoutingPresenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        // Mock scheduler using RxJava TestScheduler.
        testScheduler = new TestScheduler();
        presenter = new RoutingPresenter(routingUseCase , view , testScheduler , testScheduler);
    }


    @Test
    public void loadRouts_WhenDataIsAvailable_ShouldUpdateViews_success()
    {
        doReturn(Flowable.just(response.getRoutes())).when(routingUseCase).getRoutingResponse(true);
        presenter.getRoutes(true);
        testScheduler.triggerActions();

        verify(view).showLoading();
        verify(view).showRoutesSuccess(response.getRoutes());
        verify(view).hideLoading();
    }

    @Test
    public void loadRouts_WhenDataIsNotAvailable_ShouldUpdateViews_failure()
    {
        doReturn(Flowable.just(new Throwable(PLEASE_CHECK_INTERNET))).when(routingUseCase).
                getRoutingResponse(false);

        presenter.getRoutes(false);
        testScheduler.triggerActions();

        verify(view).showLoading();
        verify(view).showRoutesFailure(new Throwable(PLEASE_CHECK_INTERNET));
        verify(view).hideLoading();
    }
}