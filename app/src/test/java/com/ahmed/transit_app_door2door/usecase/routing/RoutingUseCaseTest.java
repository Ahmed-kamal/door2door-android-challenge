package com.ahmed.transit_app_door2door.usecase.routing;

import com.ahmed.transit_app_door2door.data.api.RoutingResponse;
import com.ahmed.transit_app_door2door.data.api.RoutingService;
import com.ahmed.transit_app_door2door.data.model.ProviderAttributes;
import com.ahmed.transit_app_door2door.data.model.Route;
import com.ahmed.transit_app_door2door.usecase.routing.remote.RoutingRemoteDataSource;
import com.google.gson.Gson;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import io.reactivex.Flowable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;

import static com.ahmed.transit_app_door2door.utils.Constants.JSON_DATA;
import static com.ahmed.transit_app_door2door.utils.Constants.PLEASE_CHECK_INTERNET;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by Ahmed Kamal on 19-12-2017.
 */
public class RoutingUseCaseTest
{
    private RoutingResponse routingResponse = new Gson().fromJson(JSON_DATA , RoutingResponse.class);

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    RoutingService routingService;

    @InjectMocks
    RoutingRemoteDataSource routingRemoteDataSource;

    @Test public void loadRouts_shouldReturnFromRemote_ifNetworkAvailable()
    {
        //When
        when(routingService.getRouting()).thenReturn(Flowable.just(routingResponse));

        TestSubscriber<List<Route>> testSubscriber =
                routingRemoteDataSource.getRoutingResponse(true).test();

        testSubscriber.awaitTerminalEvent();

        //Then
        testSubscriber
                .assertNoErrors()
                .assertValue(Objects::nonNull);
    }

    @Test public void loadRouts_shouldThrowError_ifNetworkIsNotAvailable()
    {
        //When
        when(routingService.getRouting()).thenReturn(Flowable.just(routingResponse));

        TestSubscriber<List<Route>> testSubscriber =
                routingRemoteDataSource.getRoutingResponse(false).test();

        testSubscriber.awaitTerminalEvent();

        //Then
        testSubscriber
                .assertErrorMessage(PLEASE_CHECK_INTERNET);
    }
}