package com.ahmed.transit_app_door2door;

import android.app.Application;
import android.content.Context;

import com.ahmed.transit_app_door2door.injection.AppModule;
import com.ahmed.transit_app_door2door.injection.navigation.DaggerNavigationInteractorComponent;
import com.ahmed.transit_app_door2door.injection.navigation.NavigationInteractorComponent;
import com.ahmed.transit_app_door2door.injection.routing.DaggerRoutingInteractorComponent;
import com.ahmed.transit_app_door2door.injection.routing.RoutingInteractorComponent;
import com.facebook.stetho.Stetho;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.squareup.leakcanary.LeakCanary;

import java.lang.ref.WeakReference;

import timber.log.Timber;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public class RoutingApp extends Application
{
    private RoutingInteractorComponent routingInteractorComponent;
    private NavigationInteractorComponent navigationInteractorComponent;

    private static WeakReference<Context> ctx;

    @Override
    public void onCreate()
    {
        super.onCreate();
        initializeDagger();
        AndroidThreeTen.init(this);
        ctx = new WeakReference<>(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            Stetho.initializeWithDefaults(this);
        }

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
    }

    private void initializeDagger()
    {
        routingInteractorComponent =
                DaggerRoutingInteractorComponent.builder().
                        appModule(new AppModule(this)).
                        build();
        navigationInteractorComponent =
                DaggerNavigationInteractorComponent.builder().
                        appModule(new AppModule(this)).
                        build();
    }

    public RoutingInteractorComponent getRoutingInteractorComponent()
    {
        return routingInteractorComponent;
    }

    public NavigationInteractorComponent getNavigationInteractorComponent()
    {
        return navigationInteractorComponent;
    }

    public static Context getContext()
    {
        return ctx.get();
    }

}
