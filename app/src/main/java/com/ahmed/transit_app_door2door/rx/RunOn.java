package com.ahmed.transit_app_door2door.rx;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

import static com.ahmed.transit_app_door2door.rx.SchedulerType.IO;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */
@Qualifier
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface RunOn
{
    SchedulerType value() default IO;
}
