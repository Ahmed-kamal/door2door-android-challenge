package com.ahmed.transit_app_door2door.usecase.map;

import com.ahmed.transit_app_door2door.data.model.Segment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Polyline;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Ahmed Kamal on 17/12/2017.
 */

public interface NavigationUseCase
{
    Single<GoogleMap> getPolyLine(GoogleMap googleMap , Segment segmentList);
}
