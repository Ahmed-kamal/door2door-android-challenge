package com.ahmed.transit_app_door2door.usecase.map;

import android.graphics.Color;

import com.ahmed.transit_app_door2door.R;
import com.ahmed.transit_app_door2door.RoutingApp;
import com.ahmed.transit_app_door2door.data.model.Segment;
import com.ahmed.transit_app_door2door.utils.NetworkUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

import static com.ahmed.transit_app_door2door.utils.Constants.NO_DATA_AVAIABLE;
import static com.ahmed.transit_app_door2door.utils.Constants.PLEASE_CHECK_INTERNET;

/**
 * Created by Ahmed Kamal on 17/12/2017.
 */

public class NavigationUseCaseImp implements NavigationUseCase
{
    @Override
    public Single<GoogleMap> getPolyLine(GoogleMap googleMap , Segment segment)
    {
        //Maps runs only on the main thread
        return Single.create(singleEmmiter ->
        {
            if(NetworkUtils.isConnected(RoutingApp.getContext()))
            {
                if(segment.getPolyline()!=null) {
                    List<LatLng> line = PolyUtil.decode(segment.getPolyline());
                    googleMap.addPolyline(new PolylineOptions()
                            .addAll(line)
                            .color(Color.BLACK));

                    googleMap.addMarker(new MarkerOptions().position(new LatLng(line.get(0).latitude , line.get(0).longitude)).
                            icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_man_pin)).title("Start"));

                    googleMap.addMarker(new MarkerOptions().position(new LatLng(line.get(line.size() - 1).latitude,
                            line.get(line.size() - 1).longitude)).icon(BitmapDescriptorFactory.
                            fromResource(R.drawable.ic_man_pin)).title("End"));

                    if(segment.getStops() != null)
                    {
                        if(segment.getStops().size() > 0)
                        {
                            for(int i = 0 ; i < segment.getStops().size() ; i++)
                            {
                                googleMap.addMarker(new MarkerOptions().position(new LatLng(line.get(i).latitude , line.get(i).longitude)).
                                        icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_stop)));
                            }
                        }
                    }

                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(line.get(0).
                            latitude,line.get(0).longitude), 12));

                    singleEmmiter.onSuccess(googleMap);
                }
                else {
                    singleEmmiter.onError(new Throwable(NO_DATA_AVAIABLE));
                }
            }
            else {
                singleEmmiter.onError(new Throwable(PLEASE_CHECK_INTERNET));
            }

        });
    }
}
