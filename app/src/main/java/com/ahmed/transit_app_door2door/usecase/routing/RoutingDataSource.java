package com.ahmed.transit_app_door2door.usecase.routing;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;

import com.ahmed.transit_app_door2door.data.api.RoutingResponse;
import com.ahmed.transit_app_door2door.data.model.Route;
import com.bumptech.glide.RequestBuilder;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */

public interface RoutingDataSource
{
    Flowable<List<Route>> getRoutingResponse(boolean isNetworkAvailbe);
}
