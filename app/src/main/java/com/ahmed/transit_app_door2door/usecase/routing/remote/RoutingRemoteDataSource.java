package com.ahmed.transit_app_door2door.usecase.routing.remote;


import com.ahmed.transit_app_door2door.RoutingApp;
import com.ahmed.transit_app_door2door.data.api.RoutingResponse;
import com.ahmed.transit_app_door2door.data.api.RoutingService;
import com.ahmed.transit_app_door2door.data.model.Route;
import com.ahmed.transit_app_door2door.usecase.routing.RoutingDataSource;
import com.ahmed.transit_app_door2door.utils.NetworkUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

import static com.ahmed.transit_app_door2door.utils.Constants.PLEASE_CHECK_INTERNET;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */

public class RoutingRemoteDataSource implements RoutingDataSource
{
    RoutingService routingService;

    @Inject
    public RoutingRemoteDataSource(RoutingService routingService)
    {
        this.routingService = routingService;
    }

    @Override
    public Flowable<List<Route>> getRoutingResponse(boolean isNetworkAvailable)
    {
       if(isNetworkAvailable)
       {
           return routingService.getRouting().
                   flatMapIterable(RoutingResponse::getRoutes).
                   toList().toFlowable();
       }
       else
       {
           return Flowable.error(new Throwable(PLEASE_CHECK_INTERNET));
       }
    }


}
