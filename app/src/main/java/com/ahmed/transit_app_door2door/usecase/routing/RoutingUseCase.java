package com.ahmed.transit_app_door2door.usecase.routing;


import com.ahmed.transit_app_door2door.data.model.Route;
import com.ahmed.transit_app_door2door.usecase.routing.remote.Remote;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */

public class RoutingUseCase implements RoutingDataSource
{
    private RoutingDataSource remoteRouting;
    private RoutingDataSource localRouting;

    private List<Route> caches;

    @Inject
    public RoutingUseCase(@Remote RoutingDataSource remoteRouting)
    {
        this.remoteRouting = remoteRouting;
        caches = new ArrayList<>();
    }

    @Override
    public Flowable<List<Route>> getRoutingResponse(boolean isNetworkAvailbe)
    {
        return remoteRouting.getRoutingResponse(isNetworkAvailbe).doOnComplete(() ->
        {
            // Clear cache
            // Clear data in local storage if there is available DB
            //localDataSource will have DB methods for clearing data through the DAO
        });
    }

}
