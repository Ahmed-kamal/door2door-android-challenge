package com.ahmed.transit_app_door2door.presentation.routing;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmed.transit_app_door2door.R;
import com.ahmed.transit_app_door2door.data.model.Route;
import com.ahmed.transit_app_door2door.injection.routing.DaggerRoutingComponent;
import com.ahmed.transit_app_door2door.injection.routing.RoutingPresenterModule;
import com.ahmed.transit_app_door2door.presentation.base.BaseActivity;
import com.ahmed.transit_app_door2door.presentation.routing.outerAdapters.RoutingAdapter;
import com.ahmed.transit_app_door2door.utils.NetworkUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoutingActivity extends BaseActivity implements RoutingContract.RoutingView
{

    @BindView(R.id.routes_rv)
    RecyclerView recyclerView;

    @BindView(R.id.routing_progress)
    ProgressBar progressBar;

    @BindView(R.id.error_tv)
    TextView error;

    @BindView(R.id.routing_refresh)
    SwipeRefreshLayout routingRefresh;

    @Inject
    RoutingPresenter presenter;

    private RoutingAdapter routingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routing);
        ButterKnife.bind(this);

        DaggerRoutingComponent.builder()
                .routingPresenterModule(new RoutingPresenterModule(this))
                .routingInteractorComponent(getRoutingInteractorComponent())
                .build()
                .inject(this);

        initUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.routing_menu, menu);

        // Setup search widget in action bar
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint("Search");
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override public boolean onQueryTextChange(String newText) {
                presenter.search(newText , NetworkUtils.isConnected(RoutingActivity.this));
                return true;
            }
        });

        return true;
    }

    private void initUI()
    {
        routingRefresh.setOnRefreshListener(() -> presenter.getRoutes(NetworkUtils.isConnected(this)));

        if(getSupportActionBar() != null){getSupportActionBar().setTitle(R.string.options);}

        routingAdapter = new RoutingAdapter(new ArrayList<>() , new WeakReference<>(this));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(routingAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void showRoutesSuccess(List<Route> routes)
    {
        if(routingRefresh.isRefreshing()){routingRefresh.setRefreshing(false);}
        error.setVisibility(View.GONE);
        routingAdapter.replaceData(routes);
    }

    @Override
    public void showRoutesFailure(Throwable throwable)
    {
        Toast.makeText(this ,
                throwable.getLocalizedMessage().concat(getResources().getString(R.string.retry)) , Toast.LENGTH_SHORT).show();

        if(routingRefresh.isRefreshing()){routingRefresh.setRefreshing(false);}
        error.setVisibility(View.VISIBLE);
        if(routingAdapter.getItemCount() == 0)
        {error.setText(throwable.getLocalizedMessage().concat(getResources().getString(R.string.retry)));}
    }


    @Override
    public void showLoading()
    {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading()
    {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptySearchResult()
    {
        error.setText(R.string.no_results);
    }
}
