package com.ahmed.transit_app_door2door.presentation.routing;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.ahmed.transit_app_door2door.RoutingApp;
import com.ahmed.transit_app_door2door.data.model.Route;
import com.ahmed.transit_app_door2door.rx.RunOn;
import com.ahmed.transit_app_door2door.rx.SchedulerType;
import com.ahmed.transit_app_door2door.usecase.routing.RoutingUseCase;
import com.ahmed.transit_app_door2door.utils.NetworkUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static com.ahmed.transit_app_door2door.utils.Constants.PLEASE_CHECK_INTERNET;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */

public class RoutingPresenter implements RoutingContract.RoutingPresenter , LifecycleObserver
{
    private RoutingUseCase routingUseCase;
    private RoutingContract.RoutingView view;

    private Scheduler ioScheduler;
    private Scheduler mainScheduler;

    private CompositeDisposable compositeDisposable;

    @Inject
    public RoutingPresenter(RoutingUseCase routingUseCase, RoutingContract.RoutingView view,
           @RunOn(SchedulerType.IO) Scheduler ioScheduler,@RunOn(SchedulerType.UI) Scheduler mainScheduler)
    {
        this.routingUseCase = routingUseCase;
        this.view = view;
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
        if(view instanceof LifecycleOwner) {((LifecycleOwner) view).getLifecycle().addObserver(this);}
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onAttach()
    {
        getRoutes(NetworkUtils.isConnected(RoutingApp.getContext()));
    }

    @Override
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onDetach()
    {
        compositeDisposable.clear();
    }

    @Override
    public void getRoutes(boolean isNetworkAvailable)
    {
        view.showLoading();
        addDisposable(routingUseCase.getRoutingResponse(isNetworkAvailable).
                    subscribeOn(ioScheduler).
                    observeOn(mainScheduler).
                    subscribe(this::handleRoutes , this::handleError));
    }


    @Override
    public void search(String searchItem , boolean isNetworkAvailable)
    {
        addDisposable(routingUseCase.getRoutingResponse(isNetworkAvailable)
                .debounce(100 , TimeUnit.MILLISECONDS) // if user is typing too fast
                .flatMap(Flowable::fromIterable)
                .filter(routes -> routes.getType().toLowerCase().contains(searchItem.toLowerCase()))
                .toList()
                .toFlowable()
                .subscribeOn(ioScheduler)
                .observeOn(mainScheduler)
                .subscribe(routes -> {
                    if (routes.isEmpty()) {
                        view.showEmptySearchResult();
                    } else {
                        // Update filtered data
                        view.showRoutesSuccess(routes);
                    }
                }));
    }


    private void handleRoutes(List<Route> routeList)
    {
        view.hideLoading();
        if (routeList != null && !routeList.isEmpty()) {
            view.showRoutesSuccess(routeList);
        } else {
            view.showEmptySearchResult();
        }
    }

    private void handleError(Throwable error)
    {
        view.hideLoading();
        view.showRoutesFailure(error);
    }

    private void addDisposable(Disposable disposable)
    {
        compositeDisposable.add(disposable);
    }

}
