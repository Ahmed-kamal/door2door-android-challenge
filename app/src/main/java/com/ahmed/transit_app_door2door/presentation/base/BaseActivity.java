package com.ahmed.transit_app_door2door.presentation.base;

import android.arch.lifecycle.LifecycleRegistry;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.ahmed.transit_app_door2door.RoutingApp;
import com.ahmed.transit_app_door2door.injection.navigation.NavigationInteractorComponent;
import com.ahmed.transit_app_door2door.injection.routing.RoutingInteractorComponent;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */

public abstract class BaseActivity extends AppCompatActivity
{
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    //Dagger Injection
    protected RoutingInteractorComponent getRoutingInteractorComponent() {
        return ((RoutingApp) getApplication()).getRoutingInteractorComponent();
    }

    protected NavigationInteractorComponent getNavigationInteractorComponent() {
        return ((RoutingApp) getApplication()).getNavigationInteractorComponent();
    }

    @NonNull
    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }
}
