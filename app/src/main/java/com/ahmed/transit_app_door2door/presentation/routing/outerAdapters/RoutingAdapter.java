package com.ahmed.transit_app_door2door.presentation.routing.outerAdapters;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmed.transit_app_door2door.R;
import com.ahmed.transit_app_door2door.data.model.Route;
import com.ahmed.transit_app_door2door.presentation.routing.innerAdapters.RoutingDetailsAdapter;
import com.ahmed.transit_app_door2door.utils.TextUtils;
import com.bumptech.glide.RequestBuilder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */

public class RoutingAdapter extends RecyclerView.Adapter<RoutingViewHolder>
{
    private List<Route> routes;
    private RoutingDetailsAdapter detailsAdapter;
    private WeakReference<Context> context;


    public RoutingAdapter(List<Route> routes , WeakReference<Context> context)
    {
        this.routes = routes;
        this.context = context;
    }


    @Override
    public RoutingViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.routes_list_row, parent, false);
        return new RoutingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RoutingViewHolder holder, int position)
    {
        detailsAdapter = new RoutingDetailsAdapter(routes.get(position).getSegments() , context.get());

        holder.type.setText(TextUtils.checkIfNull(routes.get(position).getType()));
        if(routes.get(position).getPrice() != null)
        {
            holder.price.setText(TextUtils.checkIfNull(String.valueOf(routes.get(position).getPrice().getAmount()) +
                    TextUtils.checkIfNull(routes.get(position).getPrice().getCurrency())));
        }

        holder.provider.setText(TextUtils.checkIfNull(routes.get(position).getProvider()));

        holder.rvDetails.setLayoutManager(new LinearLayoutManager(context.get(), LinearLayoutManager.HORIZONTAL, false));
        holder.rvDetails.setHasFixedSize(true);
        holder.rvDetails.setAdapter(detailsAdapter);
    }

    @Override
    public int getItemCount()
    {
        return routes.size();
    }

    public void replaceData(List<Route> routes)
    {
        this.routes.clear();
        this.routes.addAll(routes);
        notifyDataSetChanged();
    }

    public void updateImage()
    {

    }
}
