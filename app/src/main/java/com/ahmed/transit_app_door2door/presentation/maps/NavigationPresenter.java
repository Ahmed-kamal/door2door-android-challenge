package com.ahmed.transit_app_door2door.presentation.maps;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.ahmed.transit_app_door2door.data.model.Segment;
import com.ahmed.transit_app_door2door.rx.RunOn;
import com.ahmed.transit_app_door2door.rx.SchedulerType;
import com.ahmed.transit_app_door2door.usecase.map.NavigationUseCase;
import com.google.android.gms.maps.GoogleMap;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by Ahmed Kamal on 17/12/2017.
 */

public class NavigationPresenter implements NavigationContract.navigationPresenter , LifecycleObserver
{
    private NavigationContract.NavigationView view;
    private NavigationUseCase navigationUseCase;

    private Scheduler mainScheduler;
    private Scheduler ioScheduler;

    private CompositeDisposable compositeDisposable;

    @Inject
    public NavigationPresenter(NavigationUseCase navigationUseCase, NavigationContract.NavigationView view,
               @RunOn(SchedulerType.IO) Scheduler ioScheduler, @RunOn(SchedulerType.UI) Scheduler mainScheduler)
    {
        this.navigationUseCase = navigationUseCase;
        this.view = view;
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
        if(view instanceof LifecycleOwner) {((LifecycleOwner) view).getLifecycle().addObserver(this);}
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onAttach()
    {

    }

    @Override
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onDetach()
    {
        compositeDisposable.clear();
    }

    @Override
    public void getPolyLine(GoogleMap googleMap , Segment segment)
    {
        addDisposable(navigationUseCase.getPolyLine(googleMap , segment).
                subscribeOn(mainScheduler).
                observeOn(mainScheduler).
                subscribeWith(new DisposableSingleObserver<GoogleMap>()
        {
            @Override
            public void onSuccess(GoogleMap googleMap)
            {
                view.drawPolyLineSuccess(googleMap);
            }

            @Override
            public void onError(Throwable e)
            {
                view.drawPolyLineFailure(e.getLocalizedMessage());
            }
        }));
    }

    private void addDisposable(Disposable disposable)
    {
        this.compositeDisposable.add(disposable);
    }
}
