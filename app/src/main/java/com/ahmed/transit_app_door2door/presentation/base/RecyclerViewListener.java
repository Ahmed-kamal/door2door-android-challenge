package com.ahmed.transit_app_door2door.presentation.base;

import android.view.View;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */

public class RecyclerViewListener
{
    @FunctionalInterface
    interface OnItemClickListener
    {
        void OnItemClick(View view, int position);
    }

    @FunctionalInterface
    interface OnItemLongClickListener
    {
        void OnItemLongClick(View view, int position);
    }
}
