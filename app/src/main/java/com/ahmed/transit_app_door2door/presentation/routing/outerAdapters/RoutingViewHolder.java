package com.ahmed.transit_app_door2door.presentation.routing.outerAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ahmed.transit_app_door2door.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */

public class RoutingViewHolder extends RecyclerView.ViewHolder
{
    @BindView(R.id.routes_details_rv)
    RecyclerView rvDetails;

    @BindView(R.id.routes_details_transport_type)
    TextView type;

    @BindView(R.id.routes_details_transport_price)
    TextView price;

    @BindView(R.id.routes_details_transport_provider)
    TextView provider;

    public RoutingViewHolder(View itemView)
    {
        super(itemView);
        ButterKnife.bind(this , itemView);
    }
}
