package com.ahmed.transit_app_door2door.presentation.routing.innerAdapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmed.transit_app_door2door.R;
import com.ahmed.transit_app_door2door.data.model.Segment;
import com.ahmed.transit_app_door2door.presentation.base.BaseRecyclerViewAdapter;
import com.ahmed.transit_app_door2door.utils.SVGUtils.GlideApp;
import com.ahmed.transit_app_door2door.utils.SVGUtils.SvgSoftwareLayerSetter;
import com.bumptech.glide.RequestBuilder;

import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Ahmed Kamal on 16/12/2017.
 */

public class RoutingDetailsAdapter extends BaseRecyclerViewAdapter<RoutingDetailsViewHolder>
{
    private List<Segment> segments;
    private RequestBuilder<PictureDrawable> requestBuilder;
    private Context context;

    public RoutingDetailsAdapter(List<Segment> segments, Context context)
    {
        this.segments = segments;
        this.context = context;

        requestBuilder = GlideApp.with(context)
                .as(PictureDrawable.class)
                //.placeholder(R.drawable.image_loading)
                .error(R.drawable.image_error)
                .transition(withCrossFade())
                .listener(new SvgSoftwareLayerSetter());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.routes_details_list_row, parent, false);


        return new RoutingDetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        super.onBindViewHolder(holder, position);
        RoutingDetailsViewHolder routingDetailsViewHolder = (RoutingDetailsViewHolder) holder; //safe cast
        Segment segment = segments.get(position);

        ((RoutingDetailsViewHolder) holder).detailsClicked(context , segment);

        if(segment.getIconUrl() != null)
        {
            requestBuilder.load(Uri.parse(segment.getIconUrl())).into(routingDetailsViewHolder.mainImage);
            routingDetailsViewHolder.mainImage.setBackgroundColor(Color.parseColor(segment.getColor()));
        }
    }

    @Override
    public int getItemCount()
    {
        return segments.size();
    }
}
