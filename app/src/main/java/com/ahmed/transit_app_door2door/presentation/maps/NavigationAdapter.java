package com.ahmed.transit_app_door2door.presentation.maps;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmed.transit_app_door2door.R;
import com.ahmed.transit_app_door2door.data.model.Stop;
import com.ahmed.transit_app_door2door.presentation.base.BaseRecyclerViewAdapter;
import com.ahmed.transit_app_door2door.utils.DateTimeUtils;
import com.ahmed.transit_app_door2door.utils.TextUtils;
import com.github.vipulasri.timelineview.TimelineView;

import java.util.List;

/**
 * Created by Ahmed Kamal on 18/12/2017.
 */

public class NavigationAdapter extends BaseRecyclerViewAdapter<NavigationViewHolder>
{
    private List<Stop> stops;

    public NavigationAdapter(List<Stop> stops)
    {
        this.stops = stops;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.navigation_list_row, parent, false);
        return new NavigationViewHolder(view , viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        super.onBindViewHolder(holder, position);
        NavigationViewHolder routingDetailsViewHolder = (NavigationViewHolder) holder;
        routingDetailsViewHolder.name.setText(TextUtils.checkIfNull(stops.get(position).getName()));
        routingDetailsViewHolder.date.setText(DateTimeUtils.convertTime(stops.get(position).getDatetime()));
    }

    @Override
    public int getItemViewType(int position)
    {
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

    @Override
    public int getItemCount()
    {
        return stops.size();
    }
}
