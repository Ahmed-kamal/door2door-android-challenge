package com.ahmed.transit_app_door2door.presentation.maps;

import com.ahmed.transit_app_door2door.data.model.Segment;
import com.ahmed.transit_app_door2door.presentation.base.BasePresenter;
import com.google.android.gms.maps.GoogleMap;

/**
 * Created by Ahmed Kamal on 17/12/2017.
 */

public interface NavigationContract
{
    interface NavigationView
    {
        void drawPolyLineSuccess(GoogleMap googleMap);
        void drawPolyLineFailure(String msg);
    }

    interface navigationPresenter extends BasePresenter<NavigationView>
    {
        void getPolyLine(GoogleMap googleMap , Segment segment);
    }
}
