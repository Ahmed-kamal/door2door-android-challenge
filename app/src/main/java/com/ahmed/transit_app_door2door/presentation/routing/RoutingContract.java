package com.ahmed.transit_app_door2door.presentation.routing;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.widget.ImageView;

import com.ahmed.transit_app_door2door.data.api.RoutingResponse;
import com.ahmed.transit_app_door2door.data.model.Route;
import com.ahmed.transit_app_door2door.presentation.base.BasePresenter;
import com.bumptech.glide.RequestBuilder;

import java.util.List;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */

public interface RoutingContract
{
    interface RoutingView
    {
        void showRoutesSuccess(List<Route> routes);
        void showRoutesFailure(Throwable throwable);
        void showLoading();
        void hideLoading();
        void showEmptySearchResult();
    }

    interface RoutingPresenter extends BasePresenter<RoutingView>
    {
        void getRoutes(boolean isNetworkAvailable);
        void search(String searchItem , boolean isNetworkAvailable);
    }
}
