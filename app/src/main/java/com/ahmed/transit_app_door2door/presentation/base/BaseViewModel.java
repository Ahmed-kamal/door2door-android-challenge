package com.ahmed.transit_app_door2door.presentation.base;

import android.arch.lifecycle.ViewModel;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public final class BaseViewModel<V ,P extends BasePresenter<V>> extends ViewModel
{

    private P presenter;

    void setPresenter(P presenter) {
        if (this.presenter == null) {
            this.presenter = presenter;
        }
    }

    P getPresenter() {
        return this.presenter;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        //presenter.onPresenterDestroy();
        presenter = null;
    }
}
