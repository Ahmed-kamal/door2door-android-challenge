package com.ahmed.transit_app_door2door.presentation.maps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.ahmed.transit_app_door2door.R;
import com.ahmed.transit_app_door2door.data.model.Segment;
import com.ahmed.transit_app_door2door.injection.navigation.DaggerNavigationComponent;
import com.ahmed.transit_app_door2door.injection.navigation.NavigationPresenterModule;
import com.ahmed.transit_app_door2door.presentation.base.BaseActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ahmed.transit_app_door2door.utils.Constants.SEGMENT_PARCELABLE;

public class NavigationActivity extends BaseActivity implements NavigationContract.NavigationView ,
        OnMapReadyCallback,
        GoogleMap.OnPolylineClickListener,
        GoogleMap.OnPolygonClickListener
{

    @Inject
    NavigationPresenter presenter;

    @BindView(R.id.navigation_rv)
    RecyclerView recyclerView;

    private Segment segment;
    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);
        segment = getIntent().getParcelableExtra(SEGMENT_PARCELABLE);

        DaggerNavigationComponent.builder()
                .navigationPresenterModule(new NavigationPresenterModule(this))
                .navigationInteractorComponent(getNavigationInteractorComponent())
                .build()
                .inject(this);

        // Get the SupportMapFragment and request notification when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.navigation_map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.directions);
        }
        buildRv();
    }

    private void buildRv()
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new NavigationAdapter(segment.getStops()));
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        //Maps runs only on the main thread
        presenter.getPolyLine(googleMap , segment);
    }

    @Override
    public void drawPolyLineSuccess(GoogleMap googleMap)
    {
        googleMap.setOnPolylineClickListener(this);
        googleMap.setOnPolygonClickListener(this);
    }

    @Override
    public void drawPolyLineFailure(String msg)
    {
        Toast.makeText(this , msg , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPolygonClick(Polygon polygon)
    {

    }

    @Override
    public void onPolylineClick(Polyline polyline)
    {

    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }
}
