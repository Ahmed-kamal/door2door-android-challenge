package com.ahmed.transit_app_door2door.presentation.routing.innerAdapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.ahmed.transit_app_door2door.R;
import com.ahmed.transit_app_door2door.data.model.Segment;
import com.ahmed.transit_app_door2door.presentation.maps.NavigationActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ahmed.transit_app_door2door.utils.Constants.SEGMENT_PARCELABLE;

/**
 * Created by Ahmed Kamal on 16/12/2017.
 */

public class RoutingDetailsViewHolder extends RecyclerView.ViewHolder
{
    @BindView(R.id.routes_details_iv)
    ImageView mainImage;

    public RoutingDetailsViewHolder(View itemView)
    {
        super(itemView);
        ButterKnife.bind(this , itemView);
    }

    protected void detailsClicked(Context context , Segment segment)
    {
        mainImage.setOnClickListener(view ->
                context.startActivity(new Intent(context, NavigationActivity.class).
                        putExtra(SEGMENT_PARCELABLE , segment)));
    }
}
