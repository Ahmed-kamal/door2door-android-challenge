package com.ahmed.transit_app_door2door.presentation.base;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public interface BasePresenter<V>
{
    void onAttach();
    void onDetach();
}
