package com.ahmed.transit_app_door2door.presentation.maps;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ahmed.transit_app_door2door.R;
import com.github.vipulasri.timelineview.TimelineView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ahmed Kamal on 18/12/2017.
 */

public class NavigationViewHolder extends RecyclerView.ViewHolder
{
    @BindView(R.id.stop_name)
    TextView name;

    @BindView(R.id.stop_date)
    TextView date;

    @BindView(R.id.navigation_time_marker)
    TimelineView navigationTimeLine;


    public NavigationViewHolder(View itemView , int viewType)
    {
        super(itemView);
        ButterKnife.bind(this , itemView);
        navigationTimeLine.initLine(viewType);
    }
}
