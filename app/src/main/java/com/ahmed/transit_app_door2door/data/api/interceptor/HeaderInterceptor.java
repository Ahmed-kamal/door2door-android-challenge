package com.ahmed.transit_app_door2door.data.api.interceptor;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.ahmed.transit_app_door2door.utils.Constants.ACCEPT_HEADER;
import static com.ahmed.transit_app_door2door.utils.Constants.CONETENT_TYPE_HEADER;
import static com.ahmed.transit_app_door2door.utils.Constants.HEADER_VALUE;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public class HeaderInterceptor implements Interceptor
{
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException
    {
        Request request = chain.request();
        request = request.newBuilder()
                .addHeader(ACCEPT_HEADER, HEADER_VALUE)
                .addHeader(CONETENT_TYPE_HEADER, HEADER_VALUE)
                .build();

        return chain.proceed(request);
    }
}
