package com.ahmed.transit_app_door2door.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public class Properties
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("available_bikes")
    @Expose
    private int availableBikes;
    @SerializedName("companies")
    @Expose
    private List<Company> companies = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAvailableBikes() {
        return availableBikes;
    }

    public void setAvailableBikes(int availableBikes) {
        this.availableBikes = availableBikes;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

}
