package com.ahmed.transit_app_door2door.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public class Segment implements Parcelable
{
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("num_stops")
    @Expose
    private int numStops;
    @SerializedName("stops")
    @Expose
    private List<Stop> stops = new ArrayList<>();
    @SerializedName("travel_mode")
    @Expose
    private String travelMode;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("icon_url")
    @Expose
    private String iconUrl;
    @SerializedName("polyline")
    @Expose
    private String polyline;

    protected Segment(Parcel in)
    {
        name = in.readString();
        numStops = in.readInt();
        travelMode = in.readString();
        color = in.readString();
        iconUrl = in.readString();
        polyline = in.readString();
        in.readTypedList(stops , Stop.CREATOR);
    }

    public static final Creator<Segment> CREATOR = new Creator<Segment>()
    {
        @Override
        public Segment createFromParcel(Parcel in)
        {
            return new Segment(in);
        }

        @Override
        public Segment[] newArray(int size)
        {
            return new Segment[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumStops() {
        return numStops;
    }

    public void setNumStops(int numStops) {
        this.numStops = numStops;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getPolyline() {
        return polyline;
    }

    public void setPolyline(String polyline) {
        this.polyline = polyline;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(name);
        parcel.writeInt(numStops);
        parcel.writeString(travelMode);
        parcel.writeString(color);
        parcel.writeString(iconUrl);
        parcel.writeString(polyline);
        parcel.writeTypedList(stops);
    }
}
