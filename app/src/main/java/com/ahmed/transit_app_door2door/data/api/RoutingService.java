package com.ahmed.transit_app_door2door.data.api;

import io.reactivex.Flowable;
import io.reactivex.Single;
import retrofit2.http.GET;

import static com.ahmed.transit_app_door2door.utils.Constants.JSON_END_POINT;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public interface RoutingService
{
    @GET(JSON_END_POINT)
    Flowable<RoutingResponse> getRouting();
}
