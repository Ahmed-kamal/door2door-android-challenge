package com.ahmed.transit_app_door2door.data.api;

import com.ahmed.transit_app_door2door.data.model.ProviderAttributes;
import com.ahmed.transit_app_door2door.data.model.Route;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public class RoutingResponse
{
    @SerializedName("routes")
    @Expose
    private List<Route> routes = null;
    @SerializedName("provider_attributes")
    @Expose
    private ProviderAttributes providerAttributes;

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public ProviderAttributes getProviderAttributes() {
        return providerAttributes;
    }

    public void setProviderAttributes(ProviderAttributes providerAttributes) {
        this.providerAttributes = providerAttributes;
    }
}
