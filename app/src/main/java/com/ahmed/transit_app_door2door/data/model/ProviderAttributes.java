package com.ahmed.transit_app_door2door.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public class ProviderAttributes
{
    @SerializedName("vbb")
    @Expose
    private Vbb vbb;
    @SerializedName("driveNow")
    @Expose
    private DriveNow driveNow;
    @SerializedName("carGo")
    @Expose
    private CarGo carGo;
    @SerializedName("googleDiclaimer")
    @Expose
    private GoogleDiclaimer googleDiclaimer;
    @SerializedName("nextbike")
    @Expose
    private Nextbike nextbike;
    @SerializedName("callBike")
    @Expose
    private CallBike callBike;

    public Vbb getVbb() {
        return vbb;
    }

    public void setVbb(Vbb vbb) {
        this.vbb = vbb;
    }

    public DriveNow getDriveNow() {
        return driveNow;
    }

    public void setDriveNow(DriveNow driveNow) {
        this.driveNow = driveNow;
    }

    public CarGo getCarGo() {
        return carGo;
    }

    public void setCarGo(CarGo carGo) {
        this.carGo = carGo;
    }

    public GoogleDiclaimer getGoogleDiclaimer() {
        return googleDiclaimer;
    }

    public void setGoogleDiclaimer(GoogleDiclaimer googleDiclaimer) {
        this.googleDiclaimer = googleDiclaimer;
    }

    public Nextbike getNextbike() {
        return nextbike;
    }

    public void setNextbike(Nextbike nextbike) {
        this.nextbike = nextbike;
    }

    public CallBike getCallBike() {
        return callBike;
    }

    public void setCallBike(CallBike callBike) {
        this.callBike = callBike;
    }
}
