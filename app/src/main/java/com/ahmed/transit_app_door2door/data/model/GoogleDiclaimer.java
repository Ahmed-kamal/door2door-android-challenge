package com.ahmed.transit_app_door2door.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public class GoogleDiclaimer
{
    @SerializedName("provider_icon_url")
    @Expose
    private String providerIconUrl;
    @SerializedName("disclaimer")
    @Expose
    private String disclaimer;

    public String getProviderIconUrl() {
        return providerIconUrl;
    }

    public void setProviderIconUrl(String providerIconUrl) {
        this.providerIconUrl = providerIconUrl;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

}
