package com.ahmed.transit_app_door2door.utils;

import android.text.format.DateUtils;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import static com.ahmed.transit_app_door2door.utils.Constants.HOUR_FORMATE;
import static com.ahmed.transit_app_door2door.utils.TextUtils.NO_DATA;


/**
 * Created by Ahmed Kamal on 13/12/2017.
 */

public class DateTimeUtils
{
    public static String formatRelativeTime(long time)
    {
        return DateUtils.getRelativeTimeSpanString(time * 1000, System.currentTimeMillis(),
                android.text.format.DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public static String convertTime(String date)
    {
        if(date != null)
        {
            ZonedDateTime dateTime = ZonedDateTime.parse(date);
            return DateTimeFormatter.ofPattern(HOUR_FORMATE).format(dateTime);
        }
        else
        {
            return NO_DATA;
        }
    }
}
