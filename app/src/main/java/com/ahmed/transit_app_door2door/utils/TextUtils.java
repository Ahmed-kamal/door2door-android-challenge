package com.ahmed.transit_app_door2door.utils;

/**
 * Created by Ahmed Kamal on 16/12/2017.
 */

public class TextUtils
{
    public static final String NO_DATA = "No data available";

    public static String checkIfNull(String value)
    {
        if(value == null)
        {
            return NO_DATA;
        }
        return value;
    }
}
