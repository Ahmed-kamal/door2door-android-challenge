package com.ahmed.transit_app_door2door.injection.navigation;

import com.ahmed.transit_app_door2door.injection.rx.SchedulersModule;
import com.ahmed.transit_app_door2door.presentation.base.ActivityScope;
import com.ahmed.transit_app_door2door.presentation.maps.NavigationActivity;

import dagger.Component;

/**
 * Created by Ahmed Kamal on 18-12-2017.
 */

@ActivityScope
@Component(modules = {NavigationPresenterModule.class , SchedulersModule.class},
        dependencies = {NavigationInteractorComponent.class})

public interface NavigationComponent
{
    void inject(NavigationActivity routingActivity);
}
