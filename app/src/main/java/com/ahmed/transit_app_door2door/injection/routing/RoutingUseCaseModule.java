package com.ahmed.transit_app_door2door.injection.routing;

import com.ahmed.transit_app_door2door.usecase.map.NavigationUseCase;
import com.ahmed.transit_app_door2door.usecase.map.NavigationUseCaseImp;
import com.ahmed.transit_app_door2door.usecase.routing.RoutingDataSource;
import com.ahmed.transit_app_door2door.usecase.routing.remote.Remote;
import com.ahmed.transit_app_door2door.usecase.routing.remote.RoutingRemoteDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ahmed Kamal on 16/12/2017.
 */
@Module
public class RoutingUseCaseModule
{
    @Provides
    @Remote
    @Singleton
    public RoutingDataSource provideRemoteDataSource(RoutingRemoteDataSource routingRemoteDataSource)
    {
        return routingRemoteDataSource;
    }
}
