package com.ahmed.transit_app_door2door.injection.rx;

import com.ahmed.transit_app_door2door.rx.RunOn;
import com.ahmed.transit_app_door2door.rx.SchedulerType;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */
@Module
public class SchedulersModule
{
    @Provides
    @RunOn(SchedulerType.IO)
    Scheduler provideIO() {return Schedulers.io();}

    @Provides
    @RunOn(SchedulerType.COMPUTATION)
    Scheduler provideComputation() {
        return Schedulers.computation();
    }

    @Provides
    @RunOn(SchedulerType.UI)
    Scheduler provideUi() {
        return AndroidSchedulers.mainThread();
    }
}
