package com.ahmed.transit_app_door2door.injection.routing;

import com.ahmed.transit_app_door2door.presentation.routing.RoutingContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ahmed Kamal on 16/12/2017.
 */

@Module
public class RoutingPresenterModule
{
    private RoutingContract.RoutingView view;

    public RoutingPresenterModule(RoutingContract.RoutingView view)
    {
        this.view = view;
    }

    @Provides
    public RoutingContract.RoutingView provideView()
    {
        return view;
    }
}
