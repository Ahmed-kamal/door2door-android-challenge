package com.ahmed.transit_app_door2door.injection.routing;

import com.ahmed.transit_app_door2door.injection.rx.SchedulersModule;
import com.ahmed.transit_app_door2door.presentation.base.ActivityScope;
import com.ahmed.transit_app_door2door.presentation.routing.RoutingActivity;

import dagger.Component;

/**
 * Created by Ahmed Kamal on 16/12/2017.
 */
@ActivityScope
@Component(modules = {RoutingPresenterModule.class , SchedulersModule.class},
            dependencies = {RoutingInteractorComponent.class})

public interface RoutingComponent
{
    void inject(RoutingActivity routingActivity);
}
