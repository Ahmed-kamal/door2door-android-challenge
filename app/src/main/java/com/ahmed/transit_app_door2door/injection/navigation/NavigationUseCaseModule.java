package com.ahmed.transit_app_door2door.injection.navigation;

import com.ahmed.transit_app_door2door.usecase.map.NavigationUseCase;
import com.ahmed.transit_app_door2door.usecase.map.NavigationUseCaseImp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ahmed Kamal on 18-12-2017.
 */
@Module
public class NavigationUseCaseModule
{
    @Provides
    @Singleton
    public NavigationUseCase provideNavigationUseCase()
    {
        return new NavigationUseCaseImp();
    }
}
