package com.ahmed.transit_app_door2door.injection.navigation;

import com.ahmed.transit_app_door2door.presentation.maps.NavigationContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ahmed Kamal on 18-12-2017.
 */
@Module
public class NavigationPresenterModule
{
    private NavigationContract.NavigationView view;

    public NavigationPresenterModule(NavigationContract.NavigationView view)
    {
        this.view = view;
    }

    @Provides
    public NavigationContract.NavigationView provideView()
    {
        return view;
    }
}
