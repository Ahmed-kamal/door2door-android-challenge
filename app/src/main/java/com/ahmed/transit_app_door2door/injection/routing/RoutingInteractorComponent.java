package com.ahmed.transit_app_door2door.injection.routing;

import com.ahmed.transit_app_door2door.injection.AppModule;
import com.ahmed.transit_app_door2door.injection.service.RoutingServiceModule;
import com.ahmed.transit_app_door2door.usecase.map.NavigationUseCase;
import com.ahmed.transit_app_door2door.usecase.routing.RoutingUseCase;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Ahmed Kamal on 16/12/2017.
 */

@Singleton
@Component(modules = {RoutingUseCaseModule.class , AppModule.class , RoutingServiceModule.class})
public interface RoutingInteractorComponent
{
    RoutingUseCase provideRoutingUseCase();
}
