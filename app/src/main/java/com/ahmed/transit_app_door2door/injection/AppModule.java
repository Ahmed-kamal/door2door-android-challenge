package com.ahmed.transit_app_door2door.injection;

import android.app.Application;
import android.content.Context;

import java.lang.ref.WeakReference;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ahmed Kamal on 15/12/2017.
 */
@Module
public class AppModule
{
    private WeakReference<Context> ctx;

    public AppModule(Application context) {
        this.ctx = new WeakReference<>(context);
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return ctx.get();
    }
}
