package com.ahmed.transit_app_door2door.injection.navigation;

import com.ahmed.transit_app_door2door.injection.AppModule;
import com.ahmed.transit_app_door2door.usecase.map.NavigationUseCase;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Ahmed Kamal on 18-12-2017.
 */
@Singleton
@Component(modules = {NavigationUseCaseModule.class , AppModule.class})
public interface NavigationInteractorComponent
{
    NavigationUseCase provideNavigationUseCase();
}
